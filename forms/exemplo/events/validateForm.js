function validateForm(form){
	var msg = "";
	var count = 0;
	
	if (form.getValue("nomeCliente") == ""){
		msg += " <b>'Nome do Cliente'</b>";
		count = count + 1;
	}
	if (form.getValue("descricaoCliente") == ""){
		msg += " <b>'Descrição'</b>";
		count = count + 1;
	}
	
	//-------------------------------------------------------------------------------------------
	if (msg != "" && count > 1)
	{
		throw " Campos: "+ msg +" precisam ser preenchidos!";
	}
	if (msg != "" && count == 1)
	{
		throw " Campo: "+ msg +" precisa ser preenchido!";
	}
}